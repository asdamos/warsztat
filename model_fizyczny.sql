DROP FUNCTION check_password(text,text);

DROP TABLE car_model_part;
DROP TABLE visit_part;
DROP TABLE visit_services;
DROP TABLE services;
DROP TABLE visits;
DROP TABLE car;
DROP TABLE clients;
DROP TABLE workers;
DROP TABLE users;
DROP TABLE car_model;
DROP TABLE parts;

CREATE TABLE users(
idUser SERIAL PRIMARY KEY,
login text,
password text,
idCl int DEFAULT NULL,
idWrk int DEFAULT NULL,
permission_level int); --0 - admin, 1 - worker, 2 - client

CREATE TABLE clients(
idCl SERIAL PRIMARY KEY,
first_name text,
last_name text,
company_name text,
phone_number int);

CREATE TABLE workers(
idWrk SERIAL PRIMARY KEY,
first_name text,
last_name text,
phone_number int,
pesel int,
salary int,
adress text,
birth_date date,
employment_date date,
position text);

CREATE TABLE car_model( 
idMod SERIAL PRIMARY KEY,
model text,
brand text);

CREATE TABLE car(
idCar SERIAL PRIMARY KEY,
nrVin int,
nrReg int,
idCl int NOT NULL REFERENCES clients(idCl),
idMod int NOT NULL REFERENCES car_model(idMod));

CREATE TABLE parts(
idPart SERIAL PRIMARY KEY,
quantity int,
price int,
description text);

CREATE TABLE car_model_part(
idMod int NOT NULL REFERENCES car_model(idMod),
idPart int NOT NULL REFERENCES parts(idPart));

CREATE TABLE visits(
idVis SERIAL PRIMARY KEY,
idCar int NOT NULL REFERENCES car(idCar	),
idWrk int NOT NULL REFERENCES workers(idWrk),
work_time real,
work_finished bool DEFAULT FALSE,
visit_approved bool DEFAULT FALSE,
visit_description text,
counter_value int,
visit_date date,
visit_cost int,
labor_rate_cost int,
working_time real);

CREATE TABLE visit_part(
idVis int NOT NULL REFERENCES visits(idVis),
idPart int NOT NULL REFERENCES parts(idPart));

CREATE TABLE services(
idServ SERIAL PRIMARY KEY,
describtion text,
cost int);

CREATE TABLE visit_services(
idVis int NOT NULL REFERENCES visits(idVis),
idServ int NOT NULL REFERENCES services(idServ),
idWrk int NOT NULL REFERENCES workers(idWrk),
cost int);



INSERT INTO workers(first_name, last_name, phone_number, pesel, salary, adress, birth_date, employment_date, position)
VALUES
('Jan', 'Kowalski', NULL, NULL, NULL, NULL, NULL, NULL, NULL);


INSERT INTO users(login,password,idCl ,idWrk, permission_level) VALUES
('admin', 'admin1', NULL, NULL, 0);


INSERT INTO users(login,password,idCl ,idWrk, permission_level) VALUES
('w1', 'w1', NULL, 1, 1);


CREATE OR REPLACE FUNCTION calculate_cost(int)
	RETURNS double precision
AS $X$
	SELECT sum(parts.price) + sum(visit_services.cost) + visits.working_time * visits.labor_rate_cost
	FROM visits JOIN visit_part ON (visit_part.idVis = visits.idVis)
	JOIN parts ON (parts.idPart = visit_part.idPart)
	JOIN visit_services ON (visit_services.idVis = visits.idVis)
	WHERE visits.idVis = $1
	GROUP BY visits.idVis;
$X$ LANGUAGE SQL;

CREATE OR REPLACE FUNCTION check_password(text, text) RETURNS TABLE(id int, perm_lvl int) AS
$X$
    	DECLARE 
		idCl int;
		idWrk int;
		permission_level int;
    BEGIN
        SELECT u.idCl, u.idWrk, u.permission_level INTO idCl, idWrk, permission_level 
        FROM users u WHERE u.login=$1 AND u.password=$2;
	
	IF(permission_level IS NULL) THEN
		RETURN QUERY SELECT -1,-1;
	END IF;
	
	IF(permission_level = 0) THEN	--admin
		RETURN QUERY SELECT -1, 0;
	END IF;
		
	IF(permission_level = 1) THEN	--worker
		RETURN QUERY SELECT idWrk, 1;
	END IF;
	
	IF(permission_level = 2) THEN	--client
		RETURN QUERY SELECT idCl, 2;
	END IF;


    END;
$X$ LANGUAGE plpgsql
SECURITY DEFINER;


CREATE USER desktopApp WITH PASSWORD 'password';

GRANT EXECUTE ON FUNCTION check_password(text,text) TO desktopApp;

GRANT ALL PRIVILEGES ON car_model_idmod_seq TO desktopApp;

GRANT ALL PRIVILEGES ON parts_idpart_seq TO desktopApp;

GRANT ALL PRIVILEGES ON	clients_idcl_seq TO desktopApp;

GRANT ALL PRIVILEGES ON	services_idserv_seq TO desktopApp;

GRANT ALL PRIVILEGES ON	workers_idwrk_seq TO desktopApp;


GRANT ALL PRIVILEGES ON	car_idcar_seq TO desktopApp;

GRANT ALL PRIVILEGES ON	visits_idvis_seq TO desktopApp;

GRANT ALL PRIVILEGES ON clients TO desktopApp;
GRANT ALL PRIVILEGES ON car_model_part TO desktopApp;
GRANT ALL PRIVILEGES ON visit_part TO desktopApp;
GRANT ALL PRIVILEGES ON visit_services TO desktopApp;
GRANT ALL PRIVILEGES ON services TO desktopApp;
GRANT ALL PRIVILEGES ON visits TO desktopApp;
GRANT ALL PRIVILEGES ON car TO desktopApp;
GRANT ALL PRIVILEGES ON car_model TO desktopApp;
GRANT ALL PRIVILEGES ON parts TO desktopApp;


