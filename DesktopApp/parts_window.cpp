#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>



void MainWindow::on_actionParts_triggered()
{
    ui->stackedWidget->setCurrentIndex(2);


    ui->lineEdit_Parts_Desc->clear();
    ui->lineEdit_Parts_price->clear();
    ui->lineEdit_Parts_Quantity->clear();

    this->parts_vector.clear();
    this->car_models_vector.clear();
    ui->listWidget_Parts->clear();
    ui->listWidget_All_Cars->clear();
    ui->listWidget_Car_Part->clear();
    QSqlQuery query(db);
    query.prepare("SELECT * FROM parts");
    query.exec();
    int id_field = query.record().indexOf("idPart");
    int quantity_field = query.record().indexOf("quantity");
    int price_field = query.record().indexOf("price");
    int desc_field = query.record().indexOf("description");

    int index = 0;
    while(query.next())
    {
        int id = query.value(id_field).toInt();
        QString desc = query.value(desc_field).toString();
        int price = query.value(price_field).toInt();
        int number = query.value(quantity_field).toInt();


        struct Ui::parts_row tmp;
        tmp.id = id;
        tmp.price = price;
        tmp.quantity = number;
        tmp.desc = desc;
        tmp.index_in_widget = index;
        index++;

        this->parts_vector.push_back(tmp);
        ui->listWidget_Parts->addItem(desc);
    }

    ui->lineEdit_Parts_Desc->clear();
    ui->lineEdit_Parts_price->clear();
    ui->lineEdit_Parts_Quantity->clear();
}



void MainWindow::on_listWidget_Parts_itemSelectionChanged()
{

    int index = ui->listWidget_Parts->currentRow();
    if(index == -1)
        return;

    QString desc;
    int price;
    int number;
    int idPart;
    for(int i=0; i<this->parts_vector.size(); i++)
    {
        if(parts_vector[i].index_in_widget == index)
        {
            desc = parts_vector[i].desc;
            price = parts_vector[i].price;
            number = parts_vector[i].quantity;
            idPart = parts_vector[i].id;
            break;
        }
    }
    ui->lineEdit_Parts_Desc->setText(desc);
    ui->lineEdit_Parts_price->setText(QString::number(price));
    ui->lineEdit_Parts_Quantity->setText(QString::number(number));

    ui->listWidget_All_Cars->clear();
    ui->listWidget_Car_Part->clear();
    this->car_models_vector.clear();

    QSqlQuery query(db);
    query.prepare("SELECT * FROM car_model");
    query.exec();
    int i = 0;
    while(query.next())
    {
        int id = query.value(0).toInt();
        QString model = query.value(1).toString();
        QString brand = query.value(2).toString();
        struct Ui::car_model_row tmp;
        tmp.id = id;
        tmp.model = model;
        tmp.brand = brand;
        tmp.index_in_widget = i;
        i++;
        this->car_models_vector.push_back(tmp);
        ui->listWidget_All_Cars->addItem(brand + ", " + model);
        ui->listWidget_Car_Part->addItem(brand + ", " + model);
        ui->listWidget_Car_Part->setRowHidden(tmp.index_in_widget, true);
    }
    query.prepare("SELECT idMod FROM car_model_part WHERE car_model_part.idPart = :a");
    query.bindValue(":a", idPart);
    query.exec();
    while(query.next())
    {
        int idMod = query.value(0).toInt();

        for(int i=0; i<this->car_models_vector.size(); i++)
        {
            if(car_models_vector[i].id == idMod)
            {
                ui->listWidget_Car_Part->setRowHidden(car_models_vector[i].index_in_widget, false);
                break;
            }
        }
    }
}



void MainWindow::on_pushButton_Add_Part_clicked()
{
    QString desc = ui->lineEdit_Parts_Desc->text();
    int price = ui->lineEdit_Parts_price->text().toInt();
    int number = ui->lineEdit_Parts_Quantity->text().toInt();

    QSqlQuery query(db);
    query.prepare("INSERT INTO parts(quantity, price, description) VALUES (:a, :b, :c) RETURNING idPart");
    query.bindValue(":a", number);
    query.bindValue(":b", price);
    query.bindValue(":c", desc);

    query.exec();
    query.first();
    int idPart = query.value(0).toInt();

    int index_in_list = ui->listWidget_Parts->count();

    struct Ui::parts_row tmp;
    tmp.id = idPart;
    tmp.price = price;
    tmp.quantity = number;
    tmp.desc = desc;
    tmp.index_in_widget = index_in_list;
    this->parts_vector.push_back(tmp);
    ui->listWidget_Parts->addItem(desc);
}


void MainWindow::on_pushButton_Save_Part_clicked()
{
    QString desc = ui->lineEdit_Parts_Desc->text();
    int price = ui->lineEdit_Parts_price->text().toInt();
    int number = ui->lineEdit_Parts_Quantity->text().toInt();
    if(ui->listWidget_Parts->currentRow() == -1)
    {
        QMessageBox::information(this,"Błąd","Nie wybrano pola do edycji!");
    }
    else
    {
        //szukanie czy czasem ktos nie ma takiego auta
        QSqlQuery query(db);

        //podmiana
        int index = ui->listWidget_Parts->currentRow();
        int idPart;
        for(int i=0; i<this->parts_vector.size(); i++)
        {
            if(parts_vector[i].index_in_widget == index)
            {
                parts_vector[i].desc = desc;
                parts_vector[i].price = price;
                parts_vector[i].quantity = number;
                idPart = parts_vector[i].id;
                break;
            }
        }
        query.prepare("SELECT COUNT(*) FROM visit_part WHERE visit_part.idPart = :a");
        query.bindValue(":a", idPart);
        query.exec();
        query.first();
        int visit_part_number = query.value(0).toInt();

        if(visit_part_number >0)
        {
            QMessageBox::information(this,"Uwaga","Edytujesz część wykorzystaną przy wizytach.\nMożliwa wymagana weryfikacja");
        }

        query.prepare("UPDATE parts SET quantity=:a, price=:b, description=:c WHERE idPart=:d");
        query.bindValue(":a", number);
        query.bindValue(":b", price);
        query.bindValue(":c", desc);
        query.bindValue(":d", idPart);

        query.exec();
        ui->listWidget_Parts->currentItem()->setText(desc);
    }
}

void MainWindow::on_pushButton_Delete_Part_clicked()
{
    if(ui->listWidget_Parts->currentRow() == -1)
    {
        QMessageBox::information(this,"Błąd","Nie wybrano pola do usunięcia!");
    }
    else
    {
        QSqlQuery query(db);

        //podmiana
        int index = ui->listWidget_Parts->currentRow();
        int index_in_vector;
        int idPart;
        for(int i=0; i<this->parts_vector.size(); i++)
        {
            if(parts_vector[i].index_in_widget == index)
            {
                index_in_vector = i;
                idPart = parts_vector[i].id;
                break;
            }
        }
        query.prepare("SELECT COUNT(*) FROM visit_part WHERE visit_part.idPart = :a");
        query.bindValue(":a", idPart);
        query.exec();
        query.first();
        int part_number = query.value(0).toInt();

        if(part_number > 0)
        {
            QMessageBox::information(this,"Uwaga","Usuwasz część wykorzystaną przy wizytach.\nOperacja niedozwolona");
        }
        else
        {
            this->parts_vector.remove(index_in_vector);
            query.prepare("DELETE FROM parts WHERE idPart=:a");
            query.bindValue(":a", idPart);

            query.exec();

            query.prepare("DELETE FROM car_model_part WHERE idPart=:a");
            query.bindValue(":a", idPart);

            query.exec();


            ui->listWidget_Parts->setRowHidden(index, true);
            ui->lineEdit_Parts_Desc->clear();
            ui->lineEdit_Parts_price->clear();
            ui->lineEdit_Parts_Quantity->clear();
            ui->listWidget_All_Cars->clear();
            ui->listWidget_Car_Part->clear();
            this->car_models_vector.clear();
        }
    }
    ui->listWidget_Parts->setCurrentRow(-1);
}

void MainWindow::on_toolButton_Add_Car_Part_clicked()
{
    int current_row = ui->listWidget_All_Cars->currentRow();
    if(current_row == -1)
    {
        QMessageBox::information(this,"Błąd","Nie wybrano samochodu do dodania!");
    }
    else
    {
        int idPart;
        for(int i=0; i<this->parts_vector.size(); i++)
        {
            if(parts_vector[i].index_in_widget == ui->listWidget_Parts->currentRow())
            {
                idPart = parts_vector[i].id;
                break;
            }
        }

        for(int i=0; i<this->car_models_vector.size(); i++)
        {
            if(car_models_vector[i].index_in_widget == current_row)
            {
                if(ui->listWidget_Car_Part->isRowHidden(current_row))
                {
                        QSqlQuery query(db);
                        query.prepare("INSERT INTO car_model_part(idMod, idPart) VALUES (:a, :b)");
                        query.bindValue(":a", car_models_vector[i].id);
                        query.bindValue(":b", idPart);

                        query.exec();
                        ui->listWidget_Car_Part->setRowHidden(current_row, false);
                        break;
                }
            }
        }
    }
    ui->listWidget_All_Cars->setCurrentRow(-1);
}


void MainWindow::on_toolButton_Remove_Car_Part_clicked()
{
    int current_row = ui->listWidget_Car_Part->currentRow();
    if(current_row == -1)
    {
        QMessageBox::information(this,"Błąd","Nie wybrano samochodu do usuniecia!");
    }
    else
    {
        int idPart;
        for(int i=0; i<this->parts_vector.size(); i++)
        {
            if(parts_vector[i].index_in_widget == ui->listWidget_Parts->currentRow())
            {
                idPart = parts_vector[i].id;
                break;
            }
        }

        for(int i=0; i<this->car_models_vector.size(); i++)
        {
            if(car_models_vector[i].index_in_widget == current_row)
            {
                if(!ui->listWidget_Car_Part->isRowHidden(current_row))
                {
                        QSqlQuery query(db);
                        query.prepare("DELETE FROM car_model_part WHERE idMod = :a AND idPart = :b");
                        query.bindValue(":a", car_models_vector[i].id);
                        query.bindValue(":b", idPart);

                        query.exec();
                        ui->listWidget_Car_Part->setRowHidden(current_row, true);
                        break;
                }
            }
        }
    }
    ui->listWidget_All_Cars->setCurrentRow(-1);
}
