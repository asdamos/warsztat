#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QMessageBox>

extern QSqlDatabase db;
extern int id;
extern int perm_lvl;

namespace Ui {

struct car_model_row
{
    int id;
    QString model;
    QString brand;
    int index_in_widget;
};

struct parts_row
{
    int id;
    int price;
    int quantity;
    QString desc;
    int index_in_widget;
};

struct clients_row
{
    int id;
    int phone_number;
    QString first_name;
    QString last_name;
    QString company_name;
    int index_in_widget;
};

class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_MainWindow_destroyed();

    void on_actionHome_page_triggered();

    void on_actionCar_models_triggered();


    void on_listWidget_Car_models_itemSelectionChanged();

    void on_pushButton_Save_Car_Model_clicked();

    void on_pushButton_Add_Car_Model_clicked();

    void on_pushButton_Delete_Car_Model_clicked();

    void on_actionParts_triggered();

    void on_listWidget_Parts_itemSelectionChanged();

    void on_pushButton_Add_Part_clicked();

    void on_pushButton_Save_Part_clicked();

    void on_pushButton_Delete_Part_clicked();

    void on_toolButton_Add_Car_Part_clicked();

    void on_toolButton_Remove_Car_Part_clicked();

    void on_actionClients_triggered();

private:
    Ui::MainWindow *ui;
    QVector<struct Ui::car_model_row> car_models_vector;
    QVector<struct Ui::parts_row> parts_vector;
    QVector<struct Ui::clients_row> clients_vector;
};

#endif // MAINWINDOW_H
