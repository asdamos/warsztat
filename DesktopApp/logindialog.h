#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QtSql>
#include <QMessageBox>
#include <iostream>
#include "mainwindow.h"


extern QSqlDatabase db;
extern int id;
extern int perm_lvl;

namespace Ui {

class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

private slots:
    void on_pushButton_clicked();

    void on_LoginDialog_destroyed();

private:
    Ui::LoginDialog *ui;
    QWidget *p;
};

#endif // LOGINDIALOG_H
