#-------------------------------------------------
#
# Project created by QtCreator 2016-06-10T23:05:27
#
#-------------------------------------------------

QT       += core gui
QT       += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DesktopApp
TEMPLATE = app


SOURCES += main.cpp\
        logindialog.cpp \
    mainwindow.cpp \
    car_models_window.cpp \
    parts_window.cpp \
    clients_window.cpp

HEADERS  += logindialog.h \
    mainwindow.h

FORMS    += logindialog.ui \
    mainwindow.ui
