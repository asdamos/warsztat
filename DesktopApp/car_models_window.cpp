#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <iostream>


void MainWindow::on_actionCar_models_triggered()
{
    ui->stackedWidget->setCurrentIndex(1);
    ui->lineEdit_Brand->clear();
    ui->lineEdit_Model->clear();
    this->car_models_vector.clear();
    ui->listWidget_Car_models->clear();
    QSqlQuery query(db);
    query.prepare("SELECT * FROM car_model");
    query.exec();
    int index = 0;
    while(query.next())
    {
        int id = query.value(0).toInt();
        QString model = query.value(1).toString();
        QString brand = query.value(2).toString();
        struct Ui::car_model_row tmp;
        tmp.id = id;
        tmp.model = model;
        tmp.brand = brand;
        tmp.index_in_widget = index;
        index++;
        this->car_models_vector.push_back(tmp);
        ui->listWidget_Car_models->addItem(brand + ", " + model);
    }
}

void MainWindow::on_listWidget_Car_models_itemSelectionChanged()
{
    int index = ui->listWidget_Car_models->currentRow();
    QString model;
    QString brand;
    for(int i=0; i<this->car_models_vector.size(); i++)
    {
        if(car_models_vector[i].index_in_widget == index)
        {
            model = car_models_vector[i].model;
            brand = car_models_vector[i].brand;
            break;
        }
    }
    ui->lineEdit_Model->setText(model);
    ui->lineEdit_Brand->setText(brand);
}

void MainWindow::on_pushButton_Save_Car_Model_clicked()
{
    QString model = ui->lineEdit_Model->text();
    QString brand = ui->lineEdit_Brand->text();
    if(ui->listWidget_Car_models->currentRow() == -1)
    {
        QMessageBox::information(this,"Błąd","Nie wybrano pola do edycji!");
    }
    else
    {
        //szukanie czy czasem ktos nie ma takiego auta
        QSqlQuery query(db);



        //podmiana
        int index = ui->listWidget_Car_models->currentRow();
        int idMod;
        for(int i=0; i<this->car_models_vector.size(); i++)
        {
            if(car_models_vector[i].index_in_widget == index)
            {
                car_models_vector[i].model = model;
                car_models_vector[i].brand = brand;
                idMod = car_models_vector[i].id;
                break;
            }
        }
        query.prepare("SELECT COUNT(*) FROM car WHERE car.idMod = :a");
        query.bindValue(":a", idMod);
        query.exec();
        query.first();
        int car_number = query.value(0).toInt();

        if(car_number >0)
        {
            QMessageBox::information(this,"Uwaga","Edytujesz model posiadany przez naszych klientów.\nMożliwa wymagana weryfikacja");
        }

        query.prepare("UPDATE car_model SET model=:a, brand=:b WHERE idMod=:c");
        query.bindValue(":a", model);
        query.bindValue(":b", brand);
        query.bindValue(":c", idMod);

        query.exec();
        ui->listWidget_Car_models->currentItem()->setText(brand + ", " + model);
    }
}

void MainWindow::on_pushButton_Add_Car_Model_clicked()
{
    QString model = ui->lineEdit_Model->text();
    QString brand = ui->lineEdit_Brand->text();

    QSqlQuery query(db);
    query.prepare("INSERT INTO car_model(model, brand) VALUES (:a, :b) RETURNING idMod");
    query.bindValue(":a", model);
    query.bindValue(":b", brand);

    query.exec();
    query.first();
    int idMod = query.value(0).toInt();

    int index_in_list = ui->listWidget_Car_models->count();

    struct Ui::car_model_row tmp;
    tmp.id = idMod;
    tmp.model = model;
    tmp.brand = brand;
    tmp.index_in_widget = index_in_list;
    this->car_models_vector.push_back(tmp);
    ui->listWidget_Car_models->addItem(brand + ", " + model);
}

void MainWindow::on_pushButton_Delete_Car_Model_clicked()
{
    if(ui->listWidget_Car_models->currentRow() == -1)
    {
        QMessageBox::information(this,"Błąd","Nie wybrano pola do usunięcia!");
    }
    else
    {
        QSqlQuery query(db);

        //podmiana
        int index = ui->listWidget_Car_models->currentRow();
        int index_in_vector;
        int idMod;
        for(int i=0; i<this->car_models_vector.size(); i++)
        {
            if(car_models_vector[i].index_in_widget == index)
            {
                index_in_vector = i;
                idMod = car_models_vector[i].id;
                break;
            }
        }
        query.prepare("SELECT COUNT(*) FROM car WHERE car.idMod = :a");
        query.bindValue(":a", idMod);
        query.exec();
        query.first();
        int car_number = query.value(0).toInt();

        if(car_number >0)
        {
            QMessageBox::information(this,"Uwaga","Usuwasz model posiadany przez naszych klientów.\nOperacja niedozwolona");
        }
        else
        {
            this->car_models_vector.remove(index_in_vector);
            query.prepare("DELETE FROM car_model WHERE idMod=:a");
            query.bindValue(":a", idMod);

            query.exec();
            ui->listWidget_Car_models->setRowHidden(index, true);
            ui->lineEdit_Brand->clear();
            ui->lineEdit_Model->clear();
        }
    }
    ui->listWidget_Parts->setCurrentRow(-1);
}
