#include "logindialog.h"
#include "ui_logindialog.h"
#include <iostream>
#include <QSqlQuery>

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    p = parent;
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_pushButton_clicked()
{
    if(!db.isOpen())
    {
        QMessageBox::information(this,"Błąd","Błąd połączenia z bazą danych!");
    }
    else
    {
         QSqlQuery query(db);
         query.prepare("SELECT * FROM check_password(:a, :b)");
         query.bindValue(":a",ui->lineEditLogin->text());
         query.bindValue(":b", ui->lineEditPass->text());

         query.exec();
         query.first();
         id = query.value(0).toInt();
         perm_lvl = query.value(1).toInt();


         if(perm_lvl == -1)
         {
             QMessageBox::information(this,"Błąd","Konto nie znalezione!");
         }
         else if(perm_lvl > 1)
         {
             QMessageBox::information(this,"Błąd","Brak wystarczających uprawnień!");
         }
         else
         {
             this->close();
             p->show();
         }

    }
}

void LoginDialog::on_LoginDialog_destroyed()
{
    db.close();
}
